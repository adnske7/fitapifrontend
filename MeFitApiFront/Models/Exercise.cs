﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitApiFront.Models
{
    public class Exercise
    {
        public int ExerciseId { get; set; }
        public string Name { get; set; }
        public string TargetMuscleGroup { get; set; }
        public string Image { get; set; }
        public string VidLink { get; set; }
        public Workout Workout { get; set; }
        public int? WorkoutId { get; set; }
        public ICollection<Set>? Set { get; set; }
    }
}
