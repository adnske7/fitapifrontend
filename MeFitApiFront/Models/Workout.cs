﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitApiFront.Models
{
    public class Workout
    {
        public int WorkoutId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Complete { get; set; }
        public ICollection<Exercise> Exercise { get; set; }
        public int? ExerciseId { get; set; }
        public Profile Profile { get; set; }
        public GoalWorkout? GoalWorkout { get; set; }
        public ProgramWorkout ProgramWorkout { get; set; }
    }
}
