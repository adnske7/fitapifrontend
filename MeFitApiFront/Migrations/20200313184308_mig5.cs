﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFitApiFront.Migrations
{
    public partial class mig5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Complete",
                table: "Workout",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ExerciseId",
                table: "Workout",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExerciseId",
                table: "Workout");

            migrationBuilder.AlterColumn<bool>(
                name: "Complete",
                table: "Workout",
                type: "bit",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
