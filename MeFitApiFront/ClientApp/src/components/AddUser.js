﻿import React from 'react';
import { BrowserRouter as Router, Switch, Route, withRouter } from 'react-router-dom';





export class AddUser extends React.Component {
    state = {
        profile: {}
    }

    async PostProfile() {
        const url = 'https://localhost:44311/api/Profiles';
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.profile)
        });
        await response.json();
        await setTimeout(3000);
        this.props.history.push('/');
    }

    async registerProfile(event) {
        event.preventDefault();
        await this.setState({
            profile: {
                user: {
                    FirstName: this.state.fname.value,
                    LastName: this.state.lname.value
                },
                address: {
                    AddressLine1: this.state.address1.value,
                    AddressLine2: this.state.address2.valie,
                    AddressLine3: this.state.address3.value,
                    PostalCode: this.state.post.value,
                    City: this.state.city.value,
                    Country: this.state.country.value
                },
                Weight: this.state.weight.value,
                Height: this.state.height.value,
                MedicalConditions: this.state.medical.value,
                Disabilities: this.state.disabilities.value
            }
        })
        
        this.PostProfile();
    }

    render() {
        return (
            <div id="registerInput">
                <div id="registerBody">
                    <form onSubmit={this.registerProfile.bind(this)}>
                        <label htmlFor="fname">First name:
                   <input onChange={event => this.setState({ fname: event.target.value })} type="text" id="fname" name="fname" required /><br /><br /></label>
                        <label htmlFor="lname">Last name:
                    <input onChange={event => this.setState({ lname: event.target.value })} type="text" id="lname" name="lname" required /><br /><br /> </label>
                        <label htmlFor="address1"> Address Line 1:
                    <input onChange={event => this.setState({ address1: event.target.value })} type="text" id="address1" name="address1" required /> <br /><br /> </label>
                        <label htmlFor="address2"> Address Line 2:
                    <input onChange={event => this.setState({ address2: event.target.value })} type="text" id="address2" name="address2" /> <br /><br /> </label>
                        <label htmlFor="address3"> Address Line 3:
                    <input onChange={event => this.setState({ address3: event.target.value })} type="text" id="address2" name="address3" /> <br /><br /> </label>
                        <label htmlFor="post"> Post number:
                    <input onChange={event => this.setState({ post: event.target.value })} type="text" id="post" name="post" pattern="[0-9]{4}" title="Valid postal code" required /> <br /><br /></label>
                        <label htmlFor="city"> City:
                    <input onChange={event => this.setState({ city: event.target.value })} type="text" id="city" name="city" required /> <br /><br /></label>
                        <label htmlFor="country"> Country:
                    <input onChange={event => this.setState({ country: event.target.value })} type="text" id="country" name="country" required /> <br /><br /></label>
                        <label htmlFor="height"> Height:
                    <input onChange={event => this.setState({ height: event.target.value })} type="text" id="height" name="height" pattern="^[0-9]*" title="put height in numbers" required /> <br /><br /></label>
                        <label htmlFor="weight"> Weight:
                    <input onChange={event => this.setState({ weight: event.target.value })} type="text" id="weight" name="weight" pattern="^[0-9]*" title="put weight in numbers" required /> <br /><br /></label>
                        <label htmlFor="medical"> Medical Conditions:
                    <input onChange={event => this.setState({ medical: event.target.value })} type="text" id="medical" name="medical" /> <br /><br /></label>
                        <label htmlFor="disabilities"> Disabilities:
                    <input onChange={event => this.setState({ disabilities: event.target.value })} type="text" id="disabilities" name="disabilities" /> <br /><br /></label>
                        <input type="submit" value="Submit" />

                    </form>
                </div>


            </div>
        );

    }
}

export default withRouter(AddUser);
