﻿import React, { Component } from 'react';

export class fetchExercise extends Component {
    static displayName = fetchExercise.name;

    constructor(props) {
        super(props);
        this.state = { exercises: [], loading: true };
    }

    componentDidMount() {
        this.populateExerciseData();
    }

    static renderExercisesTable(exercises) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Exercise Id</th>
                        <th>MuscleGroup</th>
                        <th>image</th>
                        <th>vidLink</th>
                        <th>ImageLink</th>
                    </tr>
                </thead>
                <tbody>
                    {exercises.map(exercise =>
                        <tr key={exercise.exerciseId}>
                            <td>{exercise.exerciseId}</td>
                            <td>{exercise.targetMuscleGroup}</td>
                            <td>{exercise.workout.name}</td>
                            <td>{exercise.vidLink}</td>
                            <td>{exercise.image}</td>

                            <td>
                                <button className="btn btn-primary ml-2" /*onClick={() => editProfile(profile)}*/>Edit</button>
                                <button className="btn primary ml-2" /* onClick={() => deleteProfile(profile.id)}*/>Delete</button>

                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : fetchExercise.renderExercisesTable(this.state.exercises);

        return (
            <div>
                <h1 id="tabelLabel">Exercises</h1>
                {contents}
            </div>
        );
    }

    async populateExerciseData() {
        const response = await fetch('api/exercises');
        const data = await response.json();
        this.setState({ exercises: data, loading: false });
    }
}
